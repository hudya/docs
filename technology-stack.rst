======================
Hudya Technology Stack
======================

We at Hudya like technology, and we love playing around with new tools that can realise our vision in a better way. 
We also aim for a very different degree of transparancy in how we technologically deliver services, because we believe
you as a customer has a right to know how we deliver services to you and how we treat your data. This is also a great business
strategy for open-source enthusiasts as it allows us also to share our experiences with the wide technology community.
Hopefully that will also give Hudya great benefits.  

Here we describe the high-level technology choices to allow others to start down the same path and get some guidance on 
what we have found to work. We also share more specific experiences in blogs and relevant open-source projects.

We believe that the Hudya "secret sauce" can be found in so many, many factors, including how we put things together, our culture,
and all the small decisions we make. So, that makes us comfortable with sharing more than usual how we do things.

Also see our `StackShare page <https://stackshare.io/hudya>`_!

------------
Office Tools
------------
Even non-techies need to do things the right way, so we also include some information on the office tools we use.

- For email and documents, we use Office365. We treat email as non-secure, and where we need more trust/security, we sign and encrypt 
  emails. For secure attachments, we use whisp.ly, a service delivered by Boxcryptor
- Our file storage is Dropbox, but we don't like unencrypted content for sensitive data, so we encrypt all files using Boxcryptor.
- For collaboration/messaging, video meetings, and sharing of files/information across the team, we use Cisco Spark, a business 
  collaboration service with end to end encryption, thus ensuring that only users explicitly invited can read the content
- No systems are better than the authentication used. To make sure that passwords have a high standard and kept securely, we use
  LastPass. For core systems and services, we also require the use of two-factor authentication, either using Google Authenticator or 
  one-time passwords over text messages 

---------------------
Core DevOps Pipeline
---------------------
Our devops pipeline, or often referred to as CI/CD (Continuous Integration/Continuous Deployment) pipeline, is at the core of both our
development and operational activities. The build toolchain and all the systems involved in moving code from a repository into production
are considered critical assets and part of making sure we keep a supurb uptime. Here are some of the key components:

- Infrastructure as code: we use ansible extensively to provision all our core physical (or virtual) assets
- We run our services on DigitalOcean and AWS, where we aim at being able to run most services on top of both services
- We use basic hosted services, like databases, but mostly spin up our own services where needed. The trade-off is simplicity and speed
  vs vendor lock-in and lack of insights on what can go wrong
- We only run docker containers for all our services, thus all our servers are provisioned ground up to host docker containers
- Rancher is used as a container management platform, and our CI/CD pipeline deploys into Rancher
- Quay.io is our container registry
- Our build environment is Jenkins, where we pull code from our Bitbucket repositories, build the Docker containers, test them, configure
  them for the Rancher environment, push the containers to quay, and triggers upgrades in Rancher
- Rancher gives us internal DNS, an internal network with IPsec between servers, and we use Traefik as a reverse proxy to distribute requests 
  across multiple containers and servers with the same microservices, and we use Route53 
  as a dynamic DNS. This is how we get our blue/green deployments and ability to do split testing with multiple versions of services
  at the same time
- Letsencrypt gives us automatically configured certificates, fully integrated into the build pipeline

--------------
Core Web Stack
--------------

All our microservices respond only to REST APIs. This means that we don't have backdoor communication between microservices. The APIs are 
developed and documented as if they all are publicly available to third-party developers. They are not currently available publicly, but 
we plan to open up our APIs at a later point.

Our microservices are mainly built on a python, django, django REST Framework stack with mysql (Amazon RDS) as a database.

Our web apps are built on React Redux (Javascript). We started out with a boilerplate stack that generously has been made available at 
`GitHub <https://github.com/Seedstars/django-react-redux-base>`_, and have adapted it to fit our needs.
