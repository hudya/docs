**************************************
How Hudya handles personal information
**************************************

Overview
========
"Trust us", we say. But *why* should you?
When it comes to companies, we are -rightfully- sceptic. Many companies declare to do no evil and sometimes 
it is difficult to see how that can be true.
Yet, we want you to trust us. And we count on earning that trust by being transparent on what we do and how we do it.

In today's world, your personal information can hold a lot of value for many companies. So, we want you to "see 
through" our processes and to know how we use and handle it. We want you to be in control.

This section explains what information we collect, what we do to safeguard it and how we use technology to handle and 
process it.
If you are interested in the legal stuff, you can find it on our `main site <https://hudya.no/en/personvernerklaering/>`_. 

Tracking
========

Why do we track you?
--------------------
One of our main goals is to provide *smart* services. This means that we want the services we design and deliver to you 
to solve your problems proactivly. In fact and to be honest, our vision is to be able to *anticipate* problems!
In life, we have *problems* because we either do something wrong, or somebody else has done something wrong which 
affects us. And to be able to prevent problems from happening, we need to learn from experience. If we have access to 
the actions which led to a problem, the next time we see the same sequence of actions happening, we can do something to 
stop the problem from happening again. This is the reason for tracking what you do on our platform.

A real life example: if we noticed that you have visited our Customer Service page several times in the past few days, 
we could assume that you need some help and that you are not finding it there. Something like this would let us know 
that we might need to improve our Customer Service pages because you might not be finding what you are looking for, 
or because it is not clear enough.

Tools we use for tracking
-------------------------

Google Analytics
^^^^^^^^^^^^^^^^
Google Analytics is Google's website traffic analysis tool. It would be safe to say that over 80% of all websites use 
it nowadays. 
When you visit our website, Google Analytics stores a small file in your browser (called `cookie <https://simple.wikipedia.org/wiki/HTTP_cookie>`_) and this file 
lets Google Analytics keep track of the different pages of our website you visit. In broad terms, this information 
allows us to analyze how our visitors navigate our website, what pages they visit the most, and this lets us understand 
what our visitors are looking for.
You as an individual are *never* identified. We can not know your name or address, but we do know if you have visited us 
before (thanks to the cookies). 

Intercom
^^^^^^^^
Do you see that small blue-ish bubble at the bottom right of your screen? That is Intercom, our cross-platform user 
support platform. 
It serves as a communication channel between Hudya and both our potential and existing customers.
If you start a chat session with one of our sales representatives you will remain anonymous until you identify yourself 
to us by sharing your name, your e-mail address or your phone number. Until that moment, we only know that you are 
contacting us from a specific country and that your browser is set to a specific language. This is cool because it 
makes it possible for us to assign you an operator who speaks your language.

Another example of what we do with Intercom: 
Let's imagine that you are from Norway and you want to sign up for our power service: 
*If you are not a Norwegian, let us explain this: in order to sign up for a new electricity service Norwegians need to 
find their power meter and to read their current electricity meter reading.*
You have started the sign up process, you have given us your phone number, address, e-mail and you added a credit card 
which will be charged with your electricity bills. But then you get to the step where you have to enter your 
electricity meter reading and... you are not at home! You will probably quit the sign up process there and wait to 
resume it when you get back home. 
Since we keep track of how far you got into the sign up process, if after a few days we realize that you have not yet 
entered your meter reading, we will send you a friendly reminder to do so. And we will also send you some instructions 
on how to read your meter, just in case that the reason why you have not done it yet is because you don't know how to 
do it. 

During your time as our customer, we will keep track of all the different things you have done in our platform. We do 
this so that whenever you talk to one of our representatives, they will know who you are, what issues you have had 
before, and will be able to server you a lot better.


Hotjar
^^^^^^
Hotjar records *anonymous* information. We use Hotjar selectively to understand better how new parts of our applications
are used or if we have specific problems we need to investigate. When we use this tool, we are able to see how 
*somebody* used our website and what happened during their visit. We sometimes use Hotjar recordings to find 
explanations to malfunctions on our website and to discover difficulties our visitors might be experiencing, but have 
not been formally documented.
If you want to know more about how Hotjar works, you can read `How does Hotjar record visitors <https://docs.hotjar.com/v1.0/docs/how-does-hotjar-record-visitors>`_ 

---------
Login process
---------
When you create an account at Hudya, either by signing up as a supporter or to buy one of our services, we will create 
an account at Stormpath, our authentication and authorization provider.

Besides serving as a professional and secure way to store your account information, Stormpath also gives you the chance
to log in to our platform using your favourite social network account (Facebook, Twitter or LinkedIn). 
Whenever you log in with your social network account, we get access to the basic profile information you gave 
authorization to Facebook/Twitter/LinkedIn to share with third-parties. This means that if you are sharing the city you
live in on Facebook and you approve us to access your profile, we will be able to see where you live. 

----------------------------------
User-Supplied Personal Information
----------------------------------
When you create an account at Hudya, we ask you to share the following information with us: 
   * Mobile number
   * First, middle and last names
   * E-mail address
   * Birth date
   * National personal ID number - We only ask for this when it is required (and we are allowed to) for that particular 
service and country.

General information about you - like contact information - is stored in our customers database.

We have designed our platform in a way that we can only access sensitive data when we absolutely need to. A practical 
example:
When you sign up, we will ask you for your personal ID number. This piece of information will be accessible only by 
those specific parts of our platform that need it (like account creation and billing). 

----------------
Audit log
----------------
The audit log is the mechanism we have put in place to control that we handle personal information the way we have to.

Whenever you accept our terms and conditions, we record it in our audit log. So, if in the future we change something 
from those terms, we will know that we need to notify you and ask you to accept those new terms. 

The audit log is also used to explicitly track any sharing of sensitive information across the different parts of our 
platform. This allows us to audit and prove that sensitive data was not accessed (accidentally or systematically) in a 
way that is not according to the terms and conditions you have accepted.