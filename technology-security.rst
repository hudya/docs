==================
Hudya and Security
==================

Security is not something you add on top of everything else, it is an integrated design principle.
At Hudya, we worry about security in all aspects of how we develop our services.

At this point, we are not really sure what we want to share and should share about security beyond
what is covered elsewhere on this documentation site. However, we plan to add more details about
our microservices authentication and authorization, and probably more general information that
cannot be exploited in attacks.