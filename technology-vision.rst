=============================================
Hudya's Technology Vision
=============================================

**Written by Greger Teigre Wedel, Hudya CTO, and updated last time: Mar 9, 2017**

---------------------------
Trust, Cost, and Innovation
---------------------------
Hudya wants to be a trusted provider of the basic services to your home and family. Also, we want to
offer multiple products that you traditionally have to buy from different companies.
And we want to give you good prices!

In order to get all these three things, we need to do new things to reduce costs and to engage with our customers
in new ways. We don't see interaction with customers as a cost like most other companies do, instead we see interaction
with our customers as an opportunity to understand them better and get to know their needs and desires and make sure they
are happy about the services they get (any maybe want to consider other services from us).

To do this, we need to use technology in new innovative ways, and we need to make sure that our customers can
quickly find answers or solve problems themselves. This means we will have to gather information about how our customers
engage with us, the services they buy, how they use them, and what goes wrong. 

------------
Vision
------------

*Our vision is to be innovative and smart about how we use technologies like artificial intelligence, predictive analysis,
big data, and so on without freaking you out. We want to have a deep understanding how our customers interact with us, our services, and 
our technologies, and we want to quickly improve.*

----------------
Core Principles
----------------
We use three main principles as a foundation for our vision:

Code as a live cloud service
****************************
Traditional code is developed to deliver a functionality when installed on a physical box, like a server, a router, or a consumer device like a mobile phone. Along with the code is a lot of non-code needed, like build processes to convert the code from text to software that can run, a way to upgrade the software, more or less stringent processes for upgrading (typically a lot in enterprise software), as well as a support process to report bugs or issues, pull out logs, and apply fixes. 

In Hudya, we use services that run in the cloud, and we don't want upgrades, but continuous improvements. When we write code, we write it to run in a cloud environment and our developers don't leave the installation and operation of the software to others, they are responsible for the entire life-cycle of the services. This means also responding to late-night issues with a service. This approach can be referred to as an extreme form of devops, referring to how developement and operations are merged into one.

Network, servers, firewalls, and other assets as code
*****************************************************
In traditional operations, servers and other physical assets are set up and configured once and then maintained over time with new software upgrades, changing configurations to support new functionality, as well as improvements like security measures, scalability and so on. This makes the infrastructure slowly drift over time, so the services (or code) running on top of the infrastructure also has to change in order to deliver the same functionality or support the improvements of the infrastructure. This process is traditionally manual and error prone and leads to a desire to not touch things that are not broken. 

Hudya uses the principle of immutable infrastructure, also known as infrastructure as code, where code that is sorrounded by revision control, reviews, and automated tests is used to build new infrastructure when a change is done. This leads to predictable and controlled evolution of infrastructure. In real life, this would be similar to demolish your house and build up everything if you want a new kitchen. However, in the world of bits and bytes, the cost of doing this leads to lower cost of operations over time. 

Again, Hudya aims at an extreme form of immutable infrastructure where services can be delivered from multiple cloud providers across multiple data centers with dynamic transitioning of where services are delivered from.
    
User-centric loosely coupled microservices transaction architecture
*******************************************************************
Hudya relies on lots and lots of partners in each country to deliver very physical services like power, mobile, home security, as well as services with very country-specific laws, regulations, and variations. This creates a complex set of interactions with old, finicky, and sometimes manual interfaces. 

In order to handle the complexity of multiple services, multiple countries, and multiple partners (for each functionality), Hudya uses a layered architecture where the architecture core is based on REST APIs delivered by microservices that are tailored to deliver functionality for one user (at the time, i.e. user-centric) in a transaction manner. This means that all actions are triggered by an external event (transaction), like a user logging in, and the microservice code only solves a small part of the needed functionality for one user. 

Multiple microservices play together to deliver the desired functionality (loosely coupled). Interactions with external partners go through adapters that hide the legacy complexity and offers for example a power service as if the external partner supported Hudya's core architecture.  Actions that require something to happen for many users at the time, like collecting power usage information monthly, happen in separate services that uses the core functions of the microservices to insert new data. 

-----------------------
Open Source Involvement
-----------------------

There's a African proverb that resonates with our core values at Hudya: "If you want to go fast, go alone; If you want to go far, go together".  We believe that no technology would be where it is without the hard work of entire communities, and smart people building on the backs of proven giants.

To this end we believe that utilizing open source software, and contributing back wherever possible, is a key element to being able to do great things and to help others do the same.

The tools that we decide to publish will be available and documented on github:

https://github.com/hudya

Please feel free to utilize and contribute bug, feature, and pull requests where relevent.
