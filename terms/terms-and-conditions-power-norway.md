# Vilkår for kraftlevering fra Hudya Power

**Dato for siste versjon: 2017-10-31**

Hudya vil være kundens representant i strømmarkedet. Som Hudya-kunde vil du nyte godt av vår samlede forhandlermakt. Våre vilkår er utformet med tanke på at du som forbruker skal få et best mulig produkt til en best mulig opplevelse. Vi bruker standard kraftleveringsavtale som er utformet av Energi Norge med noen tilleggsvilkår som er nødvendige for å skreddersy våre kundevennlige tjenester og produkter. Dersom det er noe i disse tilleggsvilkårene som er annerledes enn i Energi Norge sine standardvilkår, gjelder det som følger av tilleggsvilkårene på de punktene. Tilleggsvilkårene er nøye utformet med tanke på kundens behov og interesse.

Standard kraftleveringsavtale finner du ved å følge [denne linken](https://www.energinorge.no/contentassets/c1d911700a3d4375baccdc611b914fd9/standard-kraftleveringsavtale-for-forbrukerkunder.pdf)

Husk at Hudya ikke har noen bindingstider eller gebyrer ved oppsigelse av avtalen.


###### **1. Generelt om vilkårene**

For å kunne kjøpe strøm fra Hudya, så må du også ha en Hudya-konto. [Brukervilkår](https://hudya.no/brukervilkar/) og [personvernerklæring](https://hudya.no/personvernerklaering/) for Hudya-kontoen gjelder i tillegg til disse vilkårene for kjøp av strøm. Dersom det er konflikt mellom disse vilkårene og vilkårene for Hudya-kontoen, vil disse vilkårene gå foran. Du vil finne de siste versjonene av disse og de generelle vilkårene på våre web sider. I disse strøm-vilkårene spesifiserer vi hvilken informasjon vi henter inn og hvordan vi benytter og deler informasjonen for strøm-tjenesten.

Når vi inngår avtale om levering av strøm, gir du Hudya fullmakt til å si opp avtalen med din nåværende strømleverandør. Oppsigelsen foregår i overensstemmelse med NVEs retningslinjer der vi i Hudya vil håndtere hele prosessen for deg som kunde. Med den samme fullmakten kan Hudya samle inn nødvendig informasjon i forbindelse med skiftet av strømleverandør fra både nettselskapet og eksisterende/ tidligere strømleverandør.

###### **2. Personopplysninger og informasjon**

I tillegg til vår generelle personvernerklæring gjelder følgende: 
For å kunne tilby deg strøm, må vi å vite adressen du ønsker faktura til, samt målepunktnummeret på strømmåleren din og adressen for hvor måleren er installert. Denne informasjonen er i sentrale registere knyttet til enten fødselsdatoen din eller personnummeret ditt. Vi ber derfor også om en av disse. For de som ikke har automatisk måler, trenger vi den nåværende målerstanden på strømmåleren for at du skal få riktig faktura fra oss og din gamle leverandør.

Fra vår partner på strøm, Energi Salg, får vi informasjon om måleren din, historisk årsforbruk, samt kWh forbruk sammen med kost-prisen pr kWh (spot). 

Vi bruker den innhentende informasjonen til å gjennomføre leverandørbyttet for deg, beregne forbruket ditt og lage månedlige regninger. Vi vil også ta kontakt med deg i forbindelse med strømbyttet eller med andre viktige nyheter eller informasjon om tjenesten vår.
Vi analyserer forbruket ditt og hvordan du bruker tjenesten for å kunne gi deg tips og bedre informasjon. Dette gjelder både informasjon som kan knyttes direkte til deg og anonymisert informasjon, avhengig av om formålet er å gi deg konkrete tips eller om formålet er å forbedre våre tjenester mer generelt.

###### **3. Betalingsvilkår**

Hudya Power tilbyr tydelige og enkle strømprodukter. Vi har ingen skjulte påslag. Prisen til deg er basert på den såkalte spot-prisen, det vi kjøper den til fra kraftbørsen, med et påslag for et obligatorisk elsertifikat og for Hudya:

-   Elsertifikater: 2,50 øre per kWt
-   Hudyas påslag: 2,90 øre per kWt

Hudya belaster deg en andel av ditt forventede forbruk forskuddsvis hver måned. Dette gjøres ved å trekke ditt kredittkort eller kort med VISA igjennom vår partner, Stripe, som ivaretar all kortinformasjon på en sikker måte. Hudya vil også etterhvert tilby andre måter å betale på.

Vi tilbyr ikke gjennomfakturering i dag, men vil tilby dette veldig snart og holder deg selvfølgelig informert om når dette er klart. Dette betyr at Hudya samler betalingen for nettjenester og elektrisk energi for deg som kunde. Strømkunder kjøper til enhver tid sine nettjenester fra det lokale nettselskapet og vil fortsatt ha et kundeforhold til dette. Med gjennomfakturering så tar vi i Hudya over faktureringsoppgavene som tidligere har vært utført av nettselskapet. Kunder som misligholder faktura med gjennomfakturering overføres til todelt fakturering og mottar egen faktura fra nettselskapet. 
Du vil belastes et a-konto beløp (samme dag som leveransen starter). Dette beløpet er bestemt med bakgrunn i ditt estimerte forbruk.

###### **4. Angrerett**

Dersom du angrer etter at du har inngått avtale med oss i Hudya har du selvfølgelig mulighet til å bruke angreretten din. Angreretten gjelder i 14 dager etter bestilling. Du laster enkelt ned vårt [angrerettskjema](https://hudya.no/wp-content/terms/angrerettsskjema.pdf), fyller ut og sender til oss på support@hudya.no.