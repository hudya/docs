
# Villkor för leverans av el från Hudya Power

##### **VÄLKOMMEN KÄRA HUDYA SUPPORTER!**

Hudya kämpar varje dag för sina Supportrar. Som kund till vår elprodukt eller andra tjänster och produkter som Hudya erbjuder, tas du automatiskt upp som medlem i vår supporterklubb och blir Hudya Supporter.

Hudya vill vara kundens representant på elmarknaden. Som Hudya Supporter får du fördelar av vår samlade förhandlingsstyrka. Våra villkor är utformade mot bakgrund av att du som konsument skall få bästa möjliga produkt till bästa möjliga upplevelse. Vi använder allmänna avtalsvillkor för ellevarantörsavtal som är utformade av Svensk Energi och Konsumentverket med vissa särskilda avtalsvillkor som är nödvändiga för att skräddarsy våra kundvänliga tjänster och produkter. I händelse av motstridiga villkor skall de särskilda avtalsvillkoren äga företräda framför de allmänna avtalsvillkoren och de särskilda villkoren är noggrant utformade med utgångspunkt i kundens behov och intresse.

De allmänna avtalsvillkoren finner du genom att följa denna länk:

[*http://www.energimarknadsbyran.se/Documents/EL2012K%20(rev).pdf?epslanguage=sv*](http://www.energimarknadsbyran.se/Documents/EL2012K%20(rev).pdf?epslanguage=sv)

Notera att Hudya inte har några bindningstider eller avgifter vid avtalens uppsägning.

##### **ALLMÄNT OM VILLKOREN** 

När vi ingår avtal om leverans av el ger du Hudya fullmakt att säga upp avtalen med din nuvarande elleverantör. Uppsägningen sker i överensstämmelse med Svensk Energi och Konsumentverkets riktlinjer där vi på Hudya vill hantera hela processen för dig som kund.

Med sådan fullmakt kan Hudya samla in nödvändig information i anslutning till bytet av elleverantör från nätbolaget och befintlig/tidigare strömleverantör. Om ditt befintliga avtal med annan elleverantör inte kan sägas upp utan att detta medför en lösenavgift, är du själv ansvarig för att betala sådan avgift.

Hudya erbjuder kundvänliga tjänster och produkter. För att kunna erbjuda detta på bästa möjliga sätt för dig samlar vi all nödvändig och relevant information och kontaktar dig via den e-postadress du väljer att kommunicera med. Som kund bör du kontrollera att Hudya har uppdaterade och korrekta personuppgifter och kontaktinformation. Vi behandlar samtliga personuppgifter i enlighet med personuppgiftslagen och riktlinjerna som framgår av punkt 2 nedan.

Om vi behöver ändra våra villkor kommer du att få besked om detta minst två månader före ändringen träder i kraft.

Om du flyttar efter att du har blivit Hudya Supporter hoppas vi att du vill fortsätta som Hudya Supporter. Om du inte önskar att fortsätta kan du naturligtvis säga upp avtalet genom att meddela oss om detta till [*supporter@hudya.se*](mailto:supporter@hudya.se) .

##### **RIKTLINJER FÖR ANVÄNDNING AV PERSONUPPGIFTER** 

Hudya skyddar dina personuppgifter.

När du registreras som kund och Hudya Supporter registreras ditt personnummer, se vidare våra [*användarvillkor*](https://hudya.se/anvandarvillkor/) och vår [*personuppgiftspolicy*](https://hudya.se/personuppgiftspolicy/). Det är viktigt för oss på Hudya att du som kund är trygg med att vi behandlar detta konfidentiellt och inte i något avseende delar detta till tredjeman, utöver vad som är nödvändigt för att tillvarata våra förpliktelser gentemot dig som kund.

Vi kommer att använda den inhämtande informationen för att fullgöra våra förpliktelser gentemot dig och leverera den service som du förväntar. Det kan handla om att genomföra leverantörsbyte, flytt av el till ny adress, betalning, informera dig om din elprodukt samt skicka dig bra erbjudanden och information om nya produkter som är en del av Hudya Groups produkt- och tjänsteutbud. Vid ingåendet av avtalet godkänner du som kund och supporter att vi kan hålla dig uppdaterad på utvecklingen av nya produkter och tjänster från Hudya Group.

Du kan när som helst dra tillbaka samtycket genom att mejla till [*supporter@hudya.se*](mailto:supporter@hudya.se) . Om upplysningarna är ofullständiga kan du naturligtvis kräva att dessa korrigeras.

##### **PRISER**

Hudya El erbjuder tydliga och enkla elprodukter. Vi har inga dolda avgifter.

-   Rörligt avtal utan fast månadsavgift och bindningstid.

-   Elpriset baseras på Hudyas självkostnadspris för spotpris från Nord Pool för aktuellt elområde samt ett tillägg för kraftinköp, elcertifikat och ursprungsmärkning, Bra Miljöval.

-   Hudyas administrativa påslag på 2,9 öre/KWh inkl moms*.*

-   Vi garanterar att du endast köper 100% förnyelsebar och ursprungsmärkt el, märkt Bra Miljöval.

Till angivet elpris tillkommer kostnader för vid var tid gällande energiskatter och mervärdesskatt. Därutöver tillkommer eventuella andra skatter, avgifter och likartade pålagor beslutade eller rekommenderade av myndighet under avtalstiden. Dessa kostnader kan justeras under innevarande avtalsperiod utan föregående underrättelse till kunden och redovisas i efterhand på fakturan. Det innebär exempelvis att prisändringar under avtalstiden kan komma at ske vid ändring av de regler och avgifter som gäller på Nord Pool, ändrad kvotplikt på elcertifikat samt för de fall Svenska Kraftnät genomför förändringarav Sveriges indelning av elområden.

##### **BETALNINGSSYSTEM OCH VILLKOR** 

Hudya debiterar dig för din elförbrukning i efterskott.

Vid kortbetalning med automatisk debitering medger du att betalning sker varje månad från det betalkort du angivit. Debitering sker i slutet av varje månad. Om betalning inte kan genomföras görs ytterligare försök kommande dagar. Om debiteringen inte lyckas erhåller du en påminnelsefaktura per e-mail eller post. Du godkänner att det belopp som debiteras på kortet varierar från månad till månad beroende på din förbrukning av el.

Avisering av vilket belopp som ska dras från ditt kort kommer att aviseras via sms eller e-mail varje månad, där hittar du även all information om din elförbrukning. Ditt medgivande gällande kortbetalning gäller tillsvidare och du kan när som helst ändra eller återkalla detta genom att kontakta Hudya via e-mail [*supporter@hudya.se*](mailto:supporter@hudya.se) eller via telefon till vår kundservice på 08-446 866 00.

Betalningen kommer att ske genom en betalningslösning via tredjepart som gör det möjligt att genomföra betalning med kreditkort och betalkort. Lösningen är snabb och säker, och betalningen blir behandlad näst intill omedelbart.

Vi erbjuder inte samfakturering för närvarande men vi kommer att erbjuda dig detta inom kort och håller dig naturligtvis informerad om när detta är tillgängligt. Samfakturering innebär att Hudya samlar betalningar för nättjänster och elhandel för dig som kund och Supporter. Elkunder köper vid var tid sina nättjänster från det lokala nätbolaget och vill fortsättningsvis ha ett kundförhållande till detta. Med samfakturering tar vi på Hudya över faktureringsuppdraget som tidigare har utförts av nätbolaget. Kunder som underlåter att betala faktura genom samlad fakturering överförs till tvådelad fakturering och mottar en egen faktura från nätbolaget.

##### **KREDITPRÖVNING**

Hudya förbehåller sig rätten att göra sedvanlig kreditprövning. Om Kunden inte blir godkänd enligt Hudyas vid var tid gällande kreditpolicy träder Avtalet inte i kraft.

##### **ÅNGERRÄTT** 

Om du ångrar ingåendet av avtalet med oss har du naturligtvis en möjlighet att använda din ångerrätt. Ångerrätten gäller i 14 dagar efter beställningen. Du laddar enkelt ner ångerrättsblanketten [*http://www.konsumentverket.se/globalassets/publikationer/kontrakt-och-mallar/angerblankett-2015-konsumentverket.pdf*](http://www.konsumentverket.se/globalassets/publikationer/kontrakt-och-mallar/angerblankett-2015-konsumentverket.pdf) , fyller i den och skickar till oss på [*supporter@hudya.se*](mailto:supporter@hudya.se).

##### **PRIS- OCH AVTALSÄNDRINGAR** 

Hudya ska, om en prishöjning har skett, underrätta Kunden om detta senast på den nästkommande betalningen. Det ska framgå när prisändringen trädde i kraft och hur priset ändrats.

Hudya förbehåller sig rätten att med sextio (60) dagars varsel ändra villkoren i Avtalet. Ändring av villkor publiceras på [*www.Hudya.se*](http://www.Hudya.se) och meddelas kunden genom e-post senast sextio (60) dagar innan ändringen träder i kraft.

##### **ÖVRIGA VILLKOR**

Meddelande från Hudya till Kunden ska anses ha kommit Kunden tillhanda omedelbart vid avsändandet om meddelandet har sänts per sms eller e-post. Om meddelandet sänts per post, inom tre dagar från avsändandet.

Om Hudya inte uppfyller sina åtaganden enligt Avtalet och detta beror på en omständighet som ligger inom Hudyas kontroll har Kunden rätt till ersättning för den skada som därvid uppstår. Kunden måste styrka skadan och dess storlek för att ersättning ska utgå. Kunden ska vidta skäliga åtgärder för att begränsa sin skada, försummar Kunden det kan ersättningen reduceras i motsvarande mån.

Rätten till ersättning omfattar inte skada hänförlig till näringsverksamhet.

Om någondera Partens fullgörande av sina åtaganden enligt Avtalet väsentligen försvåras eller förhindras av en omständighet som ligger utanför Partens kontroll och vars följder Parten inte skäligen kunnat undvika eller förhindra, ska detta utgöra befrielsegrund för ansvar för dröjsmål, skadestånd och/eller annan påföljd.

På Hudya.se finns information om dina rättigheter, hur du går tillväga för att lämna klagomål samt information om oberoende användarrådgivning. Där kan du som kund få råd om energieffektivisering och jämförelseprofiler. Där hittar du även information om vart du kan vända dig gällande information och tvistlösning.
