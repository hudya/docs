=============================================
Hudya Technical Documentation
=============================================
We care about transparency at Huyda. We believe that you as a customer has a right to know
where the money you pay us go, what our costs are, what goes to buying from partners, and
so on. But we also believe that you have a right to know HOW we give you the services, 
what we do with the information you give us (and what we don't do). 

We want you to trust us, but we recognise that trust is based on what we do, not what we
say. That is why we from day one set up a publicly available website to share with you all
the technical aspects of what we do: `http://docs.hudya.io <http://docs.hudya.io>`_

If you are not very technical, you may find the documentation here a bit ... eh...
complex, but feel free to poke around. If you are technical, you will find lots of stuff
here on how we do things and what we find important. There are things though, that is not
wise to make publicly available as it can be exploited by people with bad intent. We don't
share that information unless we know who you are and have a valid reason for getting it.
If you have any questions or feel there are things missing, please contact us in any way
available (there should be plenty, at least we try to make it easy!)

.. toctree::
    :maxdepth: 2

    technology-vision
    technology-principles
    technology-stack
    technology-security
    handling-of-information

--------------------
Terms and Conditions
--------------------

.. toctree::
    :maxdepth: 1

    terms/terms-and-conditions-power-norway
