===========================
Hudya Technology Principles
===========================

The principles found here are mostly for our employees. We have gathered here what we believe is important.

*********************************
Hudya principles for IT engineers
*********************************

- Every engineer is responsible for Hudya business goals
- Great code should make customers happy
- Three priorities: have fun, help others learn, and make great stuff
- Excellence is the long-term effect of curiosity and willingness to experiment and constantly improve
- Think "good enough", never let long-term planning get in the way from getting things done
- Don't just observe, get it done if you can
- Respectfully challenge each other, that's how we get better

******************
Product principles
******************

- A delighted customer will recommend Hudya to others, go the extra mile
- "Cut the crap": avoid unnecessary features, focus on what makes a better service for our customers
- Our products are "No manual needed", in order to understand how to make that, we want to engage with our customers
- We build lasting relationships with our customers and partners, both as Hudya and as individuals
- Understand, anticipate, and respect user emotions through their actions
- **Metrics, metrics, metrics:** Constant improvement of the customer experience and satisfaction comes from 
  objective measurement and constant improvement towards our goals

******************************
General development principles
******************************

- Clean, RESTful, well-designed APIs should drive microservices development
- Do small designs, design well, iterate
- **Automate, automate, automate** - if it's not automated, it's not done
- **Devops**: Good code behaves well in operations; good operations supports developement needs
- **Transparency:** Share your work, share your thoughts, make sure others can understand what you did and why
- **Testing:** Design tests to ensure the entire system is functionioning as designed

****************************************
Development and Microservices guidelines
****************************************

- Understand and use the concepts in the `12 factor apps <https://12factor.net/>`_
- Each service is totally stateless
- Each service should be totally isolated from other micro-services, i.e. no database sharing
- Each service is responsible for its own dependencies, i.e. no shared library that must be on latest version
- Services are designed around entities (nouns), and only one
- All interactions with the service should be through the front-door, i.e. REST API
- Each service has some responsibilites towards the external environment: 
    * same REST API style, 
    * tracking-id for each request (and for any requests to other services that are spawned as a result), 
    * use shared services like db, logging, secrets management, DNS, 
    * follow our devops deployment mechanisms and migratons
- Each service should use the REST API on the accounts service to retrieve generic accounts info, and store only own relevant data
- Each account should have a UUID used by all other microservices to store information
- Each request should have a shared and a localized tracking-id for the purposes of tracking a request through all of Hudya system[s]
- Unit tests for code integrity, automated REST API tests for end-to-end testing
 

**********************
Operational guidelines
**********************

- Ensure KPIs and SLAs are clearly defined
- Load test to ensure SLAs are met
- Dynamic scaling where possible
- Events and error messages should have clear **human-actionable** items.  If an error message fires and people ignore it, that's a failure 
  Similarly, if an error message fires, and someone has to go push a button, that should be automated
- Aim for blue/green deployments across the board.  Blue/Green shouldn't be touched by hand, dev environments are only for testing out theories 
  then automations are applied to blue/green
